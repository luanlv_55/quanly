
package models;

import controllers.StudentFormData;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.*;

@Entity
public class ClassStudent {
    @Id
    public Long id;
    public String name;
    @OneToMany(mappedBy = "classStudent")
    public List<Student> students = new ArrayList<>();

    public ClassStudent(){}

    public ClassStudent(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public static Map<String, Boolean> makeClassStudentMap(StudentFormData student) {
        Map<String, Boolean> classStudentMap = new TreeMap<String, Boolean>();
        for (ClassStudent classStudent : ClassStudent.findAll()) {
            classStudentMap.put(classStudent.getName(),(student == null) ? false : (student.classStudent != null && student.classStudent.equals(classStudent.getName())));
        }
        return classStudentMap;
    }

    public static Model.Finder<Long,ClassStudent> find = new Model.Finder<Long,ClassStudent>(Long.class, ClassStudent.class);

    public static List<ClassStudent> findAll() {
        List<ClassStudent> allClass = ClassStudent.find.all();
        return allClass;
    }

    public static ClassStudent findByName(String name){
        return find.where().eq("name", name).findUnique();
    }
}
