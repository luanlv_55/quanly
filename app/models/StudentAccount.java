package models;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class StudentAccount extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;

    @OneToOne(mappedBy = "studentAccount")
    public Student student;

    public StudentAccount() { }
    public StudentAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static StudentAccount authenticate(String email, String password) {
        return finder.where().eq("email", email).eq("password", password).findUnique();
    }

    public static void addAccount(String email){
        StudentAccount user = new StudentAccount();
        user.email = email;
        String[] pass = email.split("@");
        user.password = pass[0];
        Ebean.save(user);
    }

    public static StudentAccount findByEmail(String email){
        return finder.where().eq("email", email).findUnique();
    }

    public static void deleteByEmail(String email){
        StudentAccount user = finder.where().eq("id", email).findUnique();
        Ebean.delete(user);
    }
    public static Finder<Long, StudentAccount> finder = new Finder<Long, StudentAccount>(Long.class, StudentAccount.class);
}
