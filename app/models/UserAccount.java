package models;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class UserAccount extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;



    public UserAccount() { }
    public UserAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static UserAccount authenticate(String email, String password) {
        return finder.where().eq("email", email).eq("password", password).findUnique();
    }

    public static void addUser(String email){
        UserAccount user = new UserAccount();
        user.email = email;
        String[] pass = email.split("@");
        user.password = pass[0];
        Ebean.save(user);
    }

    public static UserAccount findByEmail(String email){
        return finder.where().eq("email", email).findUnique();
    }
    public static Finder<Long, UserAccount> finder = new Finder<Long, UserAccount>(Long.class, UserAccount.class);
}
