package models;

import com.avaje.ebean.Page;
import controllers.StudentFormData;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.*;

@Entity
public class Student extends Model implements PathBindable<Student>,
        QueryStringBindable<Student> {
    @Id
    public Long id;

    public String mssv;

    public String name;

    public Date birthDay;

    public String phoneNumber;

    public String email;



    @ManyToOne
    public ClassStudent classStudent;

    @ManyToOne
    public Faculty faculty;

    @OneToOne
    public StudentAccount studentAccount;
    public static Finder<Long, Student> find = new Finder<Long, Student>(Long.class, Student.class);

    public Student(){}

    public Student(String mssv, String name, Date birthDay){
        this.mssv = mssv;
        this.name = name;
        this.birthDay = birthDay;
    }

    public static Student makeInstance(StudentFormData formData) {
        Student student = new Student();
        student.mssv = formData.mssv;
        student.name = formData.name;
        student.birthDay = formData.birthDay;
        student.phoneNumber = formData.phoneNumber;
        student.email = formData.email;
        return student;
    }

    public String toString() {
        return String.format("%s (%s)", name, mssv);
    }

    public static Student findByMssv(String mssv) {
        return find.where().eq("mssv", mssv).findUnique();
    }

    public static Student findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    public static Page<Student> find(int page, String sortBy, String order, String filter) {
        return
                find.where()
                        .ilike("mssv", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .findPagingList(5)
                        .getPage(page);
    }

    public void delete() {
        super.delete();
    }

    @Override
    public Student bind(String key, String value) {
        return findByMssv(value);
    }

    @Override
    public F.Option<Student> bind(String key, Map<String, String[]> data) {
        return F.Option.Some(findByMssv(data.get("mssv")[0]));
    }

    @Override
    public String unbind(String s) {
        return this.mssv;
    }

    @Override
    public String javascriptUnbind() {
        return this.mssv;
    }
}
