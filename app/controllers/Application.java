package controllers;

import models.Student;
import models.StudentAccount;
import models.UserAccount;
import play.data.Form;
import play.mvc.*;

import views.html.students.dashboard;
import views.html.login;
import views.html.students.password;

import static play.data.Form.form;

public class Application extends Controller {

    public static Result index() {
        return redirect(routes.Quanly.list(0,"name","asc",""));
    }

    public static Result login() {
        String email = ctx().session().get("email");
        if (StudentAccount.findByEmail(email) != null)
            redirect(routes.Students.index(Student.findByEmail(email).mssv));
        else if (UserAccount.findByEmail(email) != null)
            return redirect(routes.Quanly.list(0,"name","asc",""));
        return ok(login.render(form(Login.class)));
    }



    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
        session().clear();
        UserAccount userAccount = UserAccount.authenticate(email, password);
        StudentAccount userAccount2 = StudentAccount.authenticate(email, password);
        if (userAccount == null && userAccount2 == null) {
            flash("error", "Invalid email and/or password");
            return redirect(routes.Application.login());
        }
        session("email", email);
        if (userAccount2 == null)
            return redirect(routes.Quanly.list(0, "name", "asc", ""));
        else
            return ok(dashboard.render(Student.findByEmail(email).name,Student.findByEmail(email).mssv));

    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }

    public static class Login {
        public String email;
        public String password;
    }
}
