package controllers;

import models.ClassStudent;
import models.Faculty;
import models.Student;
import models.StudentAccount;
import play.data.Form;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.students.dashboard;
import views.html.students.details2;
import views.html.students.password;

@Security.Authenticated(Secured.class)
public class Students extends Controller {
    private static final Form<StudentFormData> studentForm = Form.form(StudentFormData.class);

    public static Result index(String mssv){
        return ok(dashboard.render(Student.findByMssv(mssv).name, Student.findByMssv(mssv).mssv));
    }

    private static final Form<StudentAccount> accountForm = Form.form(StudentAccount.class);
    public static Result password(String mssv){
        String email = Student.findByMssv(mssv).email;
        StudentAccount studentAccount = new StudentAccount(email, "");
        Form<StudentAccount> filledForm = accountForm.fill(studentAccount);
        return ok(password.render(filledForm));
    }
    public static Result changePassword(){
        Form<StudentAccount> boundForm = accountForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct errors above.");
            return badRequest(password.render(boundForm));
        }
        StudentAccount sa = StudentAccount.findByEmail(boundForm.get().email);
        sa.password = boundForm.get().password;
        sa.update();
        return redirect(routes.Application.index());
    }

    public static Result details(String mssv) {
        Student st = Student.findByMssv(mssv);
        StudentFormData stf = new StudentFormData(st.id, st.mssv, st.name, st.birthDay, st.phoneNumber, st.email, st.classStudent.getName(), st.faculty.getName());
        Form<StudentFormData> filledForm = studentForm.fill(stf);
        return ok(details2.render(filledForm,
                Faculty.makeFacultyMap(filledForm.get()),
                ClassStudent.makeClassStudentMap(filledForm.get())));
    }

}
