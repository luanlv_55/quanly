package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import models.*;
import play.Logger;
import play.data.Form;
import play.mvc.*;
import views.html.students.dashboard;
import views.html.students.details;
import views.html.students.details2;


@Security.Authenticated(Secured.class)
public class Quanly extends Controller {
    public static Result index() {
        return redirect(routes.Quanly.list(0,"name","asc",""));
    }

    public static boolean isManager(){
        String email = ctx().session().get("email");
        return (UserAccount.findByEmail(email) != null);
    }

    public static Result list(int page, String sortBy, String order, String filter) {
        if(!isManager()){
            return ok(dashboard.render(Student.findByEmail(ctx().session().get("email")).name, Student.findByEmail(ctx().session().get("email")).mssv));
        }
        Page<Student> students = Student.find(page, sortBy, order, filter);
        return ok(views.html.catalog.render(students, sortBy, order, filter));
    }

    private static final Form<StudentFormData> studentForm = Form.form(StudentFormData.class);
    public static Result newStudent() {
        if(!isManager()){
            return ok(dashboard.render(Student.findByEmail(ctx().session().get("email")).name, Student.findByEmail(ctx().session().get("email")).mssv));
        }
        StudentFormData studentData = new StudentFormData();
        return ok(details.render(studentForm, Faculty.makeFacultyMap(studentData), ClassStudent.makeClassStudentMap(studentData)));
    }

    public static Result details(String mssv) {
//        String email = ctx().session().get("email");
//        if (email != null) {
//            UserAccount user = UserAccount.findByEmail(email);
//            if (!(user != null && (user.permission.equals("giaovu") || user.email.equals(Student.findByMssv(mssv).email)))) {
//                flash("success","Ban ko co quyen!");
//                return redirect(routes.Application.login());
//            }
//        }
        if(!isManager()){
            return ok(dashboard.render(Student.findByEmail(ctx().session().get("email")).name, Student.findByEmail(ctx().session().get("email")).mssv));
        }
        Student st = Student.findByMssv(mssv);
        StudentFormData stf = new StudentFormData(st.id, st.mssv, st.name, st.birthDay, st.phoneNumber, st.email, st.classStudent.getName(), st.faculty.getName());
        Form<StudentFormData> filledForm = studentForm.fill(stf);
        return ok(details.render(filledForm,
                Faculty.makeFacultyMap(filledForm.get()),
                ClassStudent.makeClassStudentMap(filledForm.get())));
    }

    public static Result save() {
//        String email = ctx().session().get("email");
//        if (email != null) {
//            UserAccount user = UserAccount.findByEmail(email);
//            if (!(user != null && (user.permission.equals("giaovu") || user.permission.equals("student")))) {
//                return ok("Ban khong co quyen!");
//            }
//        }
        Form<StudentFormData> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct errors above.");
            return badRequest(details.render(boundForm,
                    Faculty.makeFacultyMap(null),
                    ClassStudent.makeClassStudentMap(null)));
        }

        Student student = Student.makeInstance(boundForm.get());
        student.classStudent = ClassStudent.findByName(boundForm.get().classStudent);
        student.faculty = Faculty.findByName(boundForm.get().faculty);

        if (boundForm.get().id == null) {
            if(Student.findByMssv(student.mssv) == null) {
                StudentAccount.addAccount(boundForm.get().email);
                student.studentAccount = StudentAccount.findByEmail(boundForm.get().email);
                flash("success", String.format("Successfully added student %s", student.toString()));
                Ebean.save(student);
            }
        } else {
            student.id = boundForm.get().id;
            flash("success", String.format("Successfully update student %s", student.toString()));
            student.update();
        }
        if(StudentAccount.findByEmail(ctx().session().get("email")) != null) {
            flash("success", String.format("Successfully update!"));
            return redirect(routes.Students.details(student.mssv));
        }
        return redirect(routes.Quanly.list(0,"name","asc",""));
    }

    public static Result delete(String mssv) {
        if(!isManager()){
            return ok(dashboard.render(Student.findByEmail(ctx().session().get("email")).name, Student.findByEmail(ctx().session().get("email")).mssv));
        }
        final Student student = Student.findByMssv(mssv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.", mssv));
        }
        StudentAccount x = student.studentAccount;
        student.delete();
        x.delete();
        return redirect(routes.Quanly.list(0,"name","asc",""));
    }


}