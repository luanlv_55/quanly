package controllers;

import models.Faculty;
import models.ClassStudent;
import models.UserAccount;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import javax.validation.Constraint;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Backing class for the Student data form.
 * Requirements:
 * <ul>
 * <li> All fields are public, 
 * <li> All fields are of type String or List[String].
 * <li> A public no-arg constructor.
 * <li> A validate() method that returns null or a List[ValidationError].
 * </ul>
 */
public class StudentFormData {
    public Long id;
    public String mssv = "";
    public String name = "";
    public String phoneNumber = "";
    @Formats.DateTime(pattern="yyyy-MM-dd")
    public Date birthDay = new Date();

    public String email = "";

    public String faculty = "";

    public String classStudent = "";

    /** Required for form instantiation. */
    public StudentFormData() {
    }

    public StudentFormData(Long id, String mssv, String name, Date birthDay, String phoneNumber, String email, String classStudent, String faculty) {
        this.id = id;
        this.mssv = mssv;
        this.name = name;
        this.birthDay = birthDay;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.classStudent = classStudent;
        this.faculty = faculty;
    }

    public List<ValidationError> validate() {

        List<ValidationError> errors = new ArrayList<ValidationError>();
        if (mssv == null || mssv.length() == 0) {
            errors.add(new ValidationError("mssv", ""));
        }else if(!(mssv.matches("^[0-9]{8}$"))) {
            errors.add(new ValidationError("mssv", "MSSV must have 8 number"));
        }

        if (name == null || name.length() == 0) {
            errors.add(new ValidationError("name", "Please enter your first and last name. (required)"));
        }

        if (birthDay == null) {
            errors.add(new ValidationError("birthDay", ""));
        }

        if (!(phoneNumber == null || phoneNumber.length() == 0) &&
                !(phoneNumber.matches("^0[0-9]{9}$") || phoneNumber.matches("^0[0-9]{10}$"))) {
            errors.add(new ValidationError("phoneNumber", "Please enter correct phone number!"));
        }

        if (email == null || email.equals("")) {
            errors.add(new ValidationError("email", "Please enter email"));
        }else if (UserAccount.findByEmail(email) != null)
            errors.add(new ValidationError("email", "Existed email"));

        if (faculty == null || faculty.equals("")) {
            errors.add(new ValidationError("faculty", ""));
        }

        if (classStudent == null || classStudent.equals("")) {
            errors.add(new ValidationError("classStudent", ""));
        }
        if(errors.size() > 0)
            return errors;

        return null;
    }

}