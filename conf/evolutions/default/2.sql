# --- Sample dataset

# --- !Ups
INSERT INTO faculty VALUES (1,'Cong Nghe Thong Tin');
INSERT INTO faculty VALUES (2,'Dien Tu Vien Thong');
INSERT INTO faculty VALUES (3,'Co Dien Tu');
INSERT INTO faculty VALUES (4,'Khoa Hoc May Tinh');

INSERT INTO class_student VALUES (1,'K53');
INSERT INTO class_student VALUES (2,'K54');
INSERT INTO class_student VALUES (3,'K55');
INSERT INTO class_student VALUES (4,'K56');
INSERT INTO class_student VALUES (5,'K57');
INSERT INTO class_student VALUES (6,'K58');

INSERT INTO student_account VALUES (1002, 'hoan@example.com', 'hoan');
INSERT INTO student_account VALUES (1003, 'tinh@example.com', 'tinh');
INSERT INTO student_account VALUES (1004, 'truong@example.com', 'truong');
INSERT INTO student_account VALUES (1005, 'tung@example.com', 'tung');
INSERT INTO student_account VALUES (1006, 'vuong@example.com', 'vuong');

INSERT INTO student VALUES (1001, 12020151, 'Nguyễn Văn Hoan' , '1972-08-13', '0999999999', 'hoan@example.com', 2, 2, 1002);
INSERT INTO student VALUES (1002, 13020443, 'Phan Văn Tịnh' , '1980-07-13', '0999999999', 'tinh@example.com', 3, 1, 1003);
INSERT INTO student VALUES (1003, 13020470, 'Phạm Xuân Trường' , '1981-07-13', '0999999999', 'truong@example.com', 1, 2, 1004);
INSERT INTO student VALUES (1004, 13020488, 'Dương Đình Tùng' , '1979-07-13', '0999999999', 'tung@example.com', 2, 3, 1005);
INSERT INTO student VALUES (1005, 13020518, 'Trịnh Công Vượng' , '1961-07-13', '0999999999', 'vuong@example.com', 2, 1, 1006);


INSERT INTO user_account VALUES (1001, 'giaovu@example.com', 'giaovu');



# --- !Downs

delete from student;
delete from faculty;
delete from class_student;
delete from user_account;
delete from student_account;
