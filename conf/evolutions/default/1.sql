# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table class_student (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_class_student primary key (id))
;

create table faculty (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_faculty primary key (id))
;

create table student (
  id                        bigint not null,
  mssv                      varchar(255),
  name                      varchar(255),
  birth_day                 timestamp,
  phone_number              varchar(255),
  email                     varchar(255),
  class_student_id          bigint,
  faculty_id                bigint,
  student_account_id        bigint,
  constraint pk_student primary key (id))
;

create table student_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_student_account primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence class_student_seq;

create sequence faculty_seq;

create sequence student_seq;

create sequence student_account_seq;

create sequence user_account_seq;

alter table student add constraint fk_student_classStudent_1 foreign key (class_student_id) references class_student (id);
create index ix_student_classStudent_1 on student (class_student_id);
alter table student add constraint fk_student_faculty_2 foreign key (faculty_id) references faculty (id);
create index ix_student_faculty_2 on student (faculty_id);
alter table student add constraint fk_student_studentAccount_3 foreign key (student_account_id) references student_account (id);
create index ix_student_studentAccount_3 on student (student_account_id);



# --- !Downs

drop table if exists class_student cascade;

drop table if exists faculty cascade;

drop table if exists student cascade;

drop table if exists student_account cascade;

drop table if exists user_account cascade;

drop sequence if exists class_student_seq;

drop sequence if exists faculty_seq;

drop sequence if exists student_seq;

drop sequence if exists student_account_seq;

drop sequence if exists user_account_seq;

